<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name></name>
    <message id="whisperfish-session-has-attachment">
        <location filename="../qml/delegates/SessionDelegate.qml" line="41"/>
        <source>Attachment</source>
        <extracomment>Session contains an attachment label</extracomment>
        <translation>Anexo</translation>
    </message>
    <message id="whisperfish-session-delete-all">
        <location filename="../qml/delegates/SessionDelegate.qml" line="58"/>
        <source>All messages deleted</source>
        <oldsource>Deleting all messages</oldsource>
        <extracomment>Delete all messages from session (past tense)</extracomment>
        <translation>Todas as mensagens foram apagadas</translation>
    </message>
    <message id="whisperfish-typing-1">
        <location filename="../qml/delegates/SessionDelegate.qml" line="87"/>
        <source>%1 is typing</source>
        <extracomment>Text shown when one person is typing</extracomment>
        <translation>%1 está digitando</translation>
    </message>
    <message id="whisperfish-typing-2">
        <location filename="../qml/delegates/SessionDelegate.qml" line="91"/>
        <source>%1 and %2 are typing</source>
        <extracomment>Text shown when two persons are typing</extracomment>
        <translation>%1 e %2 estão digitando</translation>
    </message>
    <message id="whisperfish-typing-3-plus">
        <location filename="../qml/delegates/SessionDelegate.qml" line="95"/>
        <source>%1 and %n others are typing</source>
        <extracomment>Text shown when three or more persons are typing</extracomment>
        <translation>%1 e %n outros estão digitando</translation>
    </message>
    <message id="whisperfish-session-note-to-self">
        <location filename="../qml/cover/CoverPage.qml" line="117"/>
        <location filename="../qml/delegates/SessionDelegate.qml" line="32"/>
        <location filename="../qml/delegates/SessionDelegate.qml" line="183"/>
        <location filename="../qml/pages/ConversationPage.qml" line="18"/>
        <location filename="../qml/pages/ShareDestinationV1.qml" line="91"/>
        <location filename="../qml/pages/ShareDestinationV2.qml" line="90"/>
        <source>Note to self</source>
        <extracomment>Name of the conversation with one&apos;s own number</extracomment>
        <translation>Nota pessoal</translation>
    </message>
    <message id="whisperfish-message-preview-draft">
        <location filename="../qml/delegates/SessionDelegate.qml" line="202"/>
        <source>Draft: %1</source>
        <extracomment>Message preview for a saved, unsent message</extracomment>
        <translation>Rascunho: %1</translation>
    </message>
    <message id="whisperfish-session-mark-unpinned">
        <location filename="../qml/delegates/SessionDelegate.qml" line="303"/>
        <source>Unpin</source>
        <extracomment>&apos;Unpin&apos; conversation from the top of the view</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-session-mark-pinned">
        <location filename="../qml/delegates/SessionDelegate.qml" line="306"/>
        <source>Pin to top</source>
        <extracomment>&apos;Pin&apos; conversation to the top of the view</extracomment>
        <translation type="unfinished">Fixar no topo</translation>
    </message>
    <message id="whisperfish-session-mark-unmuted">
        <location filename="../qml/delegates/SessionDelegate.qml" line="315"/>
        <source>Unmute conversation</source>
        <oldsource>Mark as unmuted</oldsource>
        <extracomment>Mark conversation as unmuted</extracomment>
        <translation>Ligar notificações da conversa</translation>
    </message>
    <message id="whisperfish-session-mark-muted">
        <location filename="../qml/delegates/SessionDelegate.qml" line="318"/>
        <source>Mute conversation</source>
        <oldsource>Mark as muted</oldsource>
        <extracomment>Mark conversation as muted</extracomment>
        <translation>Desligar notificações da conversa</translation>
    </message>
    <message id="whisperfish-session-mark-unarchived">
        <location filename="../qml/delegates/SessionDelegate.qml" line="326"/>
        <source>Restore to inbox</source>
        <extracomment>Show archived messages again in the main page</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-session-mark-archived">
        <location filename="../qml/delegates/SessionDelegate.qml" line="329"/>
        <source>Archive conversation</source>
        <extracomment>Move the conversation to archived conversations</extracomment>
        <translation type="unfinished">Arquivar conversa</translation>
    </message>
    <message id="whisperfish-session-delete">
        <location filename="../qml/delegates/SessionDelegate.qml" line="336"/>
        <source>Delete conversation</source>
        <extracomment>Delete all messages from session menu</extracomment>
        <translation>Apagar conversa</translation>
    </message>
    <message id="whisperfish-notification-default-message">
        <location filename="../qml/harbour-whisperfish-main.qml" line="114"/>
        <source>New Message</source>
        <extracomment>Default label for new message notification</extracomment>
        <translation>Nova mensagem</translation>
    </message>
    <message id="whisperfish-fatal-error-setup-client">
        <location filename="../qml/harbour-whisperfish-main.qml" line="217"/>
        <source>Failed to setup Signal client</source>
        <extracomment>Failed to setup signal client error message</extracomment>
        <translation>Não foi possível configurar cliente Signal</translation>
    </message>
    <message id="whisperfish-fatal-error-invalid-datastore">
        <location filename="../qml/harbour-whisperfish-main.qml" line="222"/>
        <source>Failed to setup data storage</source>
        <oldsource>Failed to setup datastore</oldsource>
        <extracomment>Failed to setup datastore error message</extracomment>
        <translation>Não foi possível configurar o armazenamento de dados</translation>
    </message>
    <message id="permission-la-data">
        <location filename="../qml/harbour-whisperfish-main.qml" line="342"/>
        <source>Whisperfish data storage</source>
        <extracomment>Permission for Whisperfish data storage</extracomment>
        <translation>Armazenamento de dados do Whisperfish</translation>
    </message>
    <message id="permission-la-data_description">
        <location filename="../qml/harbour-whisperfish-main.qml" line="346"/>
        <source>Store configuration and messages</source>
        <extracomment>Permission description for Whisperfish data storage</extracomment>
        <translation>Armazenar definições e mensagens</translation>
    </message>
    <message id="whisperfish-show-archived-menu">
        <location filename="../qml/pages/MainPage.qml" line="92"/>
        <source>Show archived conversations</source>
        <extracomment>Menu item for showing archived conversations</extracomment>
        <translation>Mostrar conversas arquivadas</translation>
    </message>
    <message id="whisperfish-show-inbox-menu">
        <location filename="../qml/pages/MainPage.qml" line="95"/>
        <source>Return to conversations</source>
        <extracomment>Menu item for returning to &quot;inbox&quot; from archived sessions</extracomment>
        <translation>Retornar às conversas</translation>
    </message>
    <message id="whisperfish-subtitle-active-conversations">
        <location filename="../qml/pages/MainPage.qml" line="228"/>
        <source>Conversations</source>
        <extracomment>Whisperfish subtitle for active conversations aka. &quot;inbox&quot;</extracomment>
        <translation>Conversas</translation>
    </message>
    <message id="whisperfish-subtitle-archived-conversations">
        <location filename="../qml/pages/MainPage.qml" line="231"/>
        <source>Archived conversations</source>
        <extracomment>Whisperfish subtitle for archived conversations</extracomment>
        <translation>Conversas arquivadas</translation>
    </message>
    <message id="whisperfish-session-section-pinned">
        <location filename="../qml/pages/MainPage.qml" line="267"/>
        <source>Pinned</source>
        <extracomment>Session section label for pinned messages</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-session-section-today">
        <location filename="../qml/pages/MainPage.qml" line="272"/>
        <source>Today</source>
        <extracomment>Session section label for today</extracomment>
        <translation>Hoje</translation>
    </message>
    <message id="whisperfish-session-section-yesterday">
        <location filename="../qml/pages/MainPage.qml" line="277"/>
        <source>Yesterday</source>
        <extracomment>Session section label for yesterday</extracomment>
        <translation>Ontem</translation>
    </message>
    <message id="whisperfish-session-section-older">
        <location filename="../qml/pages/MainPage.qml" line="282"/>
        <source>Older</source>
        <extracomment>Session section label for older</extracomment>
        <translation>Mais antigas</translation>
    </message>
    <message id="whisperfish-about">
        <location filename="../qml/pages/About.qml" line="20"/>
        <source>About Whisperfish</source>
        <extracomment>Title for about page</extracomment>
        <translation>Sobre o Whisperfish</translation>
    </message>
    <message id="whisperfish-version">
        <location filename="../qml/pages/About.qml" line="33"/>
        <source>Whisperfish v%1</source>
        <extracomment>Whisperfish version string</extracomment>
        <translation>Whisperfish versão %1</translation>
    </message>
    <message id="whisperfish-description">
        <location filename="../qml/pages/About.qml" line="43"/>
        <source>Signal client for Sailfish OS</source>
        <extracomment>Whisperfish description</extracomment>
        <translation>Cliente Signal para S.O. Sailfish</translation>
    </message>
    <message id="whisperfish-description-section">
        <location filename="../qml/pages/About.qml" line="49"/>
        <source>Description</source>
        <extracomment>Description</extracomment>
        <translation>Descrição</translation>
    </message>
    <message id="whisperfish-long-description">
        <location filename="../qml/pages/About.qml" line="62"/>
        <source>Whisperfish is an unofficial, but advanced Signal client for Sailfish OS. Whisperfish is highly usable, but is still considered beta quality software. Make sure to update regularily! Also, check our Wiki and feel free to contribute to it! Do not ever contact the Signal developers about a Whisperfish issue, contact us instead!.</source>
        <extracomment>Whisperfish description, longer version, also for Jolla Store</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-build-id">
        <location filename="../qml/pages/About.qml" line="76"/>
        <source>Build ID: %1</source>
        <extracomment>Whisperfish long version string and build ID</extracomment>
        <translation>ID da compilação: %1</translation>
    </message>
    <message id="whisperfish-liberapay">
        <location filename="../qml/pages/About.qml" line="110"/>
        <source>Support on Liberapay</source>
        <extracomment>Support on Liberapay</extracomment>
        <translation>Apoiar pelo Liberapay</translation>
    </message>
    <message id="whisperfish-source-code">
        <location filename="../qml/pages/About.qml" line="120"/>
        <source>Source Code</source>
        <extracomment>Source Code</extracomment>
        <translation>Código fonte</translation>
    </message>
    <message id="whisperfish-bug-report">
        <location filename="../qml/pages/About.qml" line="130"/>
        <source>Report a Bug</source>
        <extracomment>Report a Bug</extracomment>
        <translation>Relatar um erro</translation>
    </message>
    <message id="whisperfish-copyright">
        <location filename="../qml/pages/About.qml" line="86"/>
        <source>Copyright</source>
        <extracomment>Copyright</extracomment>
        <translation>Direitos autorais</translation>
    </message>
    <message id="whisperfish-about-wiki-link">
        <location filename="../qml/pages/About.qml" line="140"/>
        <source>Visit the Wiki</source>
        <extracomment>Visit the Wiki button, tapping links to the Whisperfish Wiki</extracomment>
        <translation>Abrir Wiki</translation>
    </message>
    <message id="whisperfish-extra-copyright">
        <location filename="../qml/pages/About.qml" line="149"/>
        <source>Additional Copyright</source>
        <extracomment>Additional Copyright</extracomment>
        <translation>Direitos autorais adicionais</translation>
    </message>
    <message id="whisperfish-add-confirm">
        <location filename="../qml/pages/AddDevice.qml" line="25"/>
        <source>Add</source>
        <extracomment>&quot;Add&quot; message, shown in the link device dialog</extracomment>
        <translation>Adicionar</translation>
    </message>
    <message id="whisperfish-add-device">
        <location filename="../qml/pages/AddDevice.qml" line="33"/>
        <source>Add Device</source>
        <extracomment>Add Device, shown as pull-down menu item</extracomment>
        <translation>Adicionar dispositivo</translation>
    </message>
    <message id="whisperfish-device-url">
        <location filename="../qml/pages/AddDevice.qml" line="43"/>
        <source>Device URL</source>
        <extracomment>Device URL, text input for pasting the QR-scanned code</extracomment>
        <translation>Endereço do dispositivo</translation>
    </message>
    <message id="whisperfish-device-link-instructions">
        <location filename="../qml/pages/AddDevice.qml" line="76"/>
        <source>Install Signal Desktop. Use the CodeReader application to scan the QR code displayed on Signal Desktop and copy and paste the URL here.</source>
        <extracomment>Instructions on how to scan QR code for device linking</extracomment>
        <translation>Instale o Signal Desktop. Use a aplicação CodeReader para ler o código QR exibido no Signal Desktop e copie o endereço aqui.</translation>
    </message>
    <message id="whisperfish-select-picture">
        <location filename="../qml/pages/ImagePicker.qml" line="44"/>
        <source>Select picture</source>
        <extracomment>Title for image picker page</extracomment>
        <translation>Escolha a imagem</translation>
    </message>
    <message id="whisperfish-add-linked-device">
        <location filename="../qml/pages/LinkedDevices.qml" line="17"/>
        <source>Add</source>
        <extracomment>Menu option to add new linked device</extracomment>
        <translation>Adicionar</translation>
    </message>
    <message id="whisperfish-refresh-linked-devices">
        <location filename="../qml/pages/LinkedDevices.qml" line="30"/>
        <source>Refresh</source>
        <extracomment>Menu option to refresh linked devices</extracomment>
        <translation>Atualizar</translation>
    </message>
    <message id="whisperfish-linked-devices">
        <location filename="../qml/pages/LinkedDevices.qml" line="39"/>
        <source>Linked Devices</source>
        <extracomment>Title for Linked Devices page</extracomment>
        <translation>Dispositivos associados</translation>
    </message>
    <message id="whisperfish-device-unlink-message">
        <location filename="../qml/pages/LinkedDevices.qml" line="49"/>
        <source>Unlinked</source>
        <oldsource>Unlinking</oldsource>
        <extracomment>Unlinking remorse info message for unlinking secondary devices (past tense)</extracomment>
        <translation>Não associado</translation>
    </message>
    <message id="whisperfish-current-device-name">
        <location filename="../qml/pages/LinkedDevices.qml" line="66"/>
        <source>Current device (Whisperfish, %1)</source>
        <extracomment>Linked device title for current Whisperfish</extracomment>
        <translation>Dispositivo atual (Whisperfish, %1)</translation>
    </message>
    <message id="whisperfish-device-name">
        <location filename="../qml/pages/LinkedDevices.qml" line="70"/>
        <source>Device %1</source>
        <extracomment>Linked device name</extracomment>
        <translation>Dispositivo %1</translation>
    </message>
    <message id="whisperfish-device-link-date">
        <location filename="../qml/pages/LinkedDevices.qml" line="84"/>
        <source>Linked: %1</source>
        <extracomment>Linked device date</extracomment>
        <translation>Associado: %1</translation>
    </message>
    <message id="whisperfish-device-last-active">
        <location filename="../qml/pages/LinkedDevices.qml" line="103"/>
        <source>Last active: %1</source>
        <extracomment>Linked device last active date</extracomment>
        <translation>Última atividade: %1</translation>
    </message>
    <message id="whisperfish-device-unlink">
        <location filename="../qml/pages/LinkedDevices.qml" line="125"/>
        <source>Unlink</source>
        <extracomment>Device unlink menu option</extracomment>
        <translation>Desassociar</translation>
    </message>
    <message id="whisperfish-no-messages-hint-text">
        <location filename="../qml/pages/MainPage.qml" line="251"/>
        <source>Pull down to start a new conversation.</source>
        <extracomment>No messages found, hint on what to do</extracomment>
        <translation>Deslize para baixo para iniciar uma nova conversa.</translation>
    </message>
    <message id="whisperfish-about-menu">
        <location filename="../qml/pages/MainPage.qml" line="56"/>
        <source>About Whisperfish</source>
        <extracomment>About whisperfish menu item</extracomment>
        <translation>Sobre o Whisperfish</translation>
    </message>
    <message id="whisperfish-update-reminder-summary">
        <location filename="../qml/pages/MainPage.qml" line="22"/>
        <source>Please check for updates</source>
        <extracomment>Update notification title text</extracomment>
        <translation>Verifique por favor para atualizações</translation>
    </message>
    <message id="whisperfish-update-reminder-body">
        <location filename="../qml/pages/MainPage.qml" line="26"/>
        <source>This Whisperfish release is more than 90 days old. Please check for an update in order to keep Whisperfish running smoothly.</source>
        <extracomment>About whisperfish menu item</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-settings-menu">
        <location filename="../qml/pages/MainPage.qml" line="62"/>
        <source>Settings</source>
        <extracomment>Whisperfish settings menu item</extracomment>
        <translation>Configurações</translation>
    </message>
    <message id="whisperfish-new-message-menu">
        <location filename="../qml/pages/MainPage.qml" line="113"/>
        <source>New Message</source>
        <extracomment>Whisperfish new message menu item</extracomment>
        <translation>Nova mensagem</translation>
    </message>
    <message id="whisperfish-registration-required-message">
        <location filename="../qml/pages/MainPage.qml" line="243"/>
        <source>Registration required</source>
        <extracomment>Whisperfish registration required message</extracomment>
        <translation>É necessário ter conta</translation>
    </message>
    <message id="whisperfish-locked-message">
        <location filename="../qml/pages/MainPage.qml" line="247"/>
        <source>Locked</source>
        <extracomment>Whisperfish locked message</extracomment>
        <translation>Bloqueado</translation>
    </message>
    <message id="whisperfish-remorse-deleted-messages-locally" numerus="yes">
        <location filename="../qml/components/MessagesView.qml" line="142"/>
        <source>Locally deleted %n message(s)</source>
        <oldsource>Locally deleted %1 messages</oldsource>
        <extracomment>Remorse: *locally* deleted one or multiple message (past tense)</extracomment>
        <translation>
            <numerusform>Toque para não apagar mensagem localmente</numerusform>
            <numerusform>Toque para não apagar localmente %n mensagens</numerusform>
        </translation>
    </message>
    <message id="whisperfish-react-message-menu">
        <location filename="../qml/components/MessagesView.qml" line="367"/>
        <source>React</source>
        <extracomment>React with emoji to message menu item</extracomment>
        <translation>Reagir</translation>
    </message>
    <message id="whisperfish-copy-message-menu">
        <location filename="../qml/components/MessagesView.qml" line="381"/>
        <source>Copy</source>
        <extracomment>Copy message menu item</extracomment>
        <translation>Copiar</translation>
    </message>
    <message id="whisperfish-forward-message-menu">
        <location filename="../qml/components/MessagesView.qml" line="388"/>
        <source>Forward</source>
        <extracomment>Forward message menu item</extracomment>
        <translation>Encaminhar</translation>
    </message>
    <message id="whisperfish-select-or-options-message-menu">
        <location filename="../qml/components/MessagesView.qml" line="395"/>
        <source>Select • more</source>
        <extracomment>&quot;Select and show more options&quot; message menu item</extracomment>
        <translation>Selecionar • mais</translation>
    </message>
    <message id="whisperfish-resend-message-menu">
        <location filename="../qml/components/MessagesView.qml" line="374"/>
        <source>Retry sending</source>
        <oldsource>Resend</oldsource>
        <extracomment>Resend message menu item</extracomment>
        <translation>Tentar reenviar</translation>
    </message>
    <message id="whisperfish-reset-identity-menu">
        <location filename="../qml/pages/VerifyIdentity.qml" line="20"/>
        <source>Reset identity key</source>
        <extracomment>Reset identity key menu item</extracomment>
        <translation type="unfinished">Reinicializar chave de identidade</translation>
    </message>
    <message id="whisperfish-reset-identity-message">
        <location filename="../qml/pages/VerifyIdentity.qml" line="25"/>
        <source>Identity key reset</source>
        <extracomment>Reset identity key remorse message (past tense)</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-reset-session-menu">
        <location filename="../qml/pages/VerifyIdentity.qml" line="35"/>
        <source>Reset Secure Session</source>
        <extracomment>Reset secure session menu item</extracomment>
        <translation>Reiniciar sessão segura</translation>
    </message>
    <message id="whisperfish-reset-session-message">
        <location filename="../qml/pages/VerifyIdentity.qml" line="40"/>
        <source>Secure session reset</source>
        <oldsource>Resetting secure session</oldsource>
        <extracomment>Reset secure session remorse message (past tense)</extracomment>
        <translation>Toque para não reiniciar sessão segura</translation>
    </message>
    <message id="whisperfish-refresh-profile-menu">
        <location filename="../qml/pages/VerifyIdentity.qml" line="50"/>
        <source>Refresh Signal profile</source>
        <extracomment>Refresh contact profile menu item</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-show-contact-page-menu">
        <location filename="../qml/pages/VerifyIdentity.qml" line="59"/>
        <source>Show contact</source>
        <extracomment>Show a peer&apos;s system contact page (menu item)</extracomment>
        <translation>Mostrar contato</translation>
    </message>
    <message id="whisperfish-numeric-fingerprint-directions">
        <location filename="../qml/pages/VerifyIdentity.qml" line="130"/>
        <source>If you wish to verify the security of your end-to-end encryption with %1, compare the numbers above with the numbers on their device.</source>
        <extracomment>Numeric fingerprint instructions</extracomment>
        <translation>Compare os números acima com os que aparecem no dispositivo de %1 para certificar-se que criptografia ponta-a-ponta é segura.</translation>
    </message>
    <message id="whisperfish-recipient-number-invalid-chars">
        <location filename="../qml/pages/NewMessage.qml" line="59"/>
        <source>This phone number contains invalid characters.</source>
        <extracomment>invalid recipient phone number: invalid characters</extracomment>
        <translation>Este número de telefone contém caracteres inválidos.</translation>
    </message>
    <message id="whisperfish-recipient-local-number-not-allowed">
        <location filename="../qml/pages/NewMessage.qml" line="64"/>
        <source>Please set a country code in the settings, or use the international format.</source>
        <extracomment>invalid recipient phone number: local numbers are not allowed</extracomment>
        <translation>Por favor defina o seu país nas definições ou utilize o formato telefônico internacional.</translation>
    </message>
    <message id="whisperfish-recipient-number-invalid-unspecified">
        <location filename="../qml/pages/NewMessage.qml" line="68"/>
        <source>This phone number appears to be invalid.</source>
        <extracomment>invalid recipient phone number: failed to format</extracomment>
        <translation>Este número de telefone não é válido.</translation>
    </message>
    <message id="whisperfish-new-message-title">
        <location filename="../qml/pages/NewMessage.qml" line="96"/>
        <source>New message</source>
        <extracomment>New message page title</extracomment>
        <translation>Nova mensagem</translation>
    </message>
    <message id="whisperfish-new-group-title">
        <location filename="../qml/pages/NewGroup.qml" line="38"/>
        <source>New Group</source>
        <extracomment>New group page title</extracomment>
        <translation>Novo grupo</translation>
    </message>
    <message id="whisperfish-group-name-label">
        <location filename="../qml/pages/NewGroup.qml" line="47"/>
        <source>Group Name</source>
        <extracomment>Group name label</extracomment>
        <translation>Nome do grupo</translation>
    </message>
    <message id="whisperfish-group-name-placeholder">
        <location filename="../qml/pages/NewGroup.qml" line="50"/>
        <source>Group Name</source>
        <extracomment>Group name placeholder</extracomment>
        <translation>Amigos e família</translation>
    </message>
    <message id="whisperfish-new-group-message-members">
        <location filename="../qml/pages/NewGroup.qml" line="69"/>
        <location filename="../qml/pages/NewGroup.qml" line="73"/>
        <source>Members</source>
        <extracomment>New group message members label
----------
Summary of all selected recipients, e.g. &quot;Bob, Jane, 75553243&quot;</extracomment>
        <translation>Membros</translation>
    </message>
    <message id="whisperfish-new-message-recipient">
        <location filename="../qml/pages/NewMessage.qml" line="114"/>
        <location filename="../qml/pages/NewMessage.qml" line="118"/>
        <source>Recipient</source>
        <extracomment>New message recipient label
----------
Summary of all selected recipients, e.g. &quot;Bob, Jane, 75553243&quot;</extracomment>
        <translation>Destinatário</translation>
    </message>
    <message id="whisperfish-error-invalid-group-name">
        <location filename="../qml/pages/NewGroup.qml" line="110"/>
        <source>Please name the group</source>
        <extracomment>Invalid group name error</extracomment>
        <translation>Por favor insira um nome de grupo válido</translation>
    </message>
    <message id="whisperfish-error-invalid-group-members">
        <location filename="../qml/pages/NewGroup.qml" line="106"/>
        <source>Please select group members</source>
        <extracomment>Invalid recipient error</extracomment>
        <translation>Por favor selecione membros para o grupo</translation>
    </message>
    <message id="whisperfish-error-invalid-recipient">
        <location filename="../qml/pages/NewMessage.qml" line="174"/>
        <source>Invalid recipient</source>
        <extracomment>Invalid recipient error</extracomment>
        <translation>Destinatário inválido</translation>
    </message>
    <message id="whisperfish-initial-setup-welcome-title">
        <location filename="../qml/pages/SetupPasswordPage.qml" line="16"/>
        <source>Welcome to Whisperfish</source>
        <extracomment>welcome screen title when creating a new database</extracomment>
        <translation>Bem-vindo ao Whisperfish</translation>
    </message>
    <message id="whisperfish-setup-password-prompt">
        <location filename="../qml/pages/SetupPasswordPage.qml" line="20"/>
        <source>Set a new password to secure your conversations.</source>
        <extracomment>new password setup prompt</extracomment>
        <translation>Define uma senha nova para tornar as conversas seguras.</translation>
    </message>
    <message id="whisperfish-password-label-too-short">
        <location filename="../qml/pages/SetupPasswordPage.qml" line="71"/>
        <location filename="../qml/pages/SetupPasswordPage.qml" line="102"/>
        <source>Password is too short</source>
        <extracomment>Password label when too short</extracomment>
        <translation>Senha muito curta</translation>
    </message>
    <message id="whisperfish-password-label">
        <location filename="../qml/pages/SetupPasswordPage.qml" line="74"/>
        <location filename="../qml/pages/UnlockPage.qml" line="73"/>
        <source>Password</source>
        <extracomment>Password label</extracomment>
        <translation>Senha</translation>
    </message>
    <message id="whisperfish-new-password-placeholder">
        <location filename="../qml/pages/SetupPasswordPage.qml" line="77"/>
        <source>Your new password</source>
        <extracomment>New password input placeholder</extracomment>
        <translation>Insira sua senha aqui</translation>
    </message>
    <message id="whisperfish-password-repeated-label">
        <location filename="../qml/pages/SetupPasswordPage.qml" line="97"/>
        <source>Repeat the password</source>
        <oldsource>Repeated password</oldsource>
        <extracomment>repeated password input label</extracomment>
        <translation>Repetir senha</translation>
    </message>
    <message id="whisperfish-password-repeated-label-wrong">
        <location filename="../qml/pages/SetupPasswordPage.qml" line="105"/>
        <source>Passwords do not match</source>
        <extracomment>repeated password input label if passwords don&apos;t match</extracomment>
        <translation>As senhas inseridas são diferentes</translation>
    </message>
    <message id="whisperfish-new-password-repeat-placeholder">
        <location filename="../qml/pages/SetupPasswordPage.qml" line="109"/>
        <source>Repeat your new password</source>
        <extracomment>Repeated new password input placeholder</extracomment>
        <translation>Repita sua nova senha</translation>
    </message>
    <message id="whisperfish-skip-button-label">
        <location filename="../qml/pages/SetupPasswordPage.qml" line="135"/>
        <source>Skip</source>
        <extracomment>skip button label</extracomment>
        <translation>Pular</translation>
    </message>
    <message id="whisperfish-unlock-page-title">
        <location filename="../qml/pages/UnlockPage.qml" line="9"/>
        <source>Unlock</source>
        <extracomment>unlock page title</extracomment>
        <translation>Desbloquear</translation>
    </message>
    <message id="whisperfish-unlock-welcome-title">
        <location filename="../qml/pages/UnlockPage.qml" line="12"/>
        <source>Whisperfish</source>
        <extracomment>unlock page welcome title, centered on screen</extracomment>
        <translation>Whisperfish</translation>
    </message>
    <message id="whisperfish-unlock-password-prompt">
        <location filename="../qml/pages/UnlockPage.qml" line="15"/>
        <source>Please enter your password to unlock your conversations.</source>
        <extracomment>unlock page password prompt</extracomment>
        <translation>Digite a senha para desbloquear conversas.</translation>
    </message>
    <message id="whisperfish-registration-secondary-title">
        <location filename="../qml/pages/RegisterSecondaryPage.qml" line="13"/>
        <source>Link as secondary device</source>
        <extracomment>register as secondary device qr page title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-register-linked-message">
        <location filename="../qml/pages/RegisterSecondaryPage.qml" line="17"/>
        <source>Please scan the QR code below using the Signal app.</source>
        <extracomment>User instructions</extracomment>
        <translation type="unfinished">Por favor, digitalize o código QR abaixo usando o aplicativo Signal.</translation>
    </message>
    <message id="whisperfish-fatal-error-msg-not-registered">
        <location filename="../qml/pages/RegisterSecondaryPage.qml" line="33"/>
        <location filename="../qml/pages/UnlockPage.qml" line="27"/>
        <location filename="../qml/pages/VerifyRegistrationPage.qml" line="67"/>
        <source>You are not registered.</source>
        <extracomment>fatal error when trying to unlock the db when not registered</extracomment>
        <translation>Sua conta não existe.</translation>
    </message>
    <message id="whisperfish-unlock-try-again">
        <location filename="../qml/pages/UnlockPage.qml" line="52"/>
        <source>Please try again</source>
        <extracomment>input field placeholder after failed attempt to unlock (keep it short)</extracomment>
        <translation>Tente novamente</translation>
    </message>
    <message id="whisperfish-password-placeholder">
        <location filename="../qml/pages/UnlockPage.qml" line="76"/>
        <source>Your password</source>
        <oldsource>Password</oldsource>
        <extracomment>password placeholder</extracomment>
        <translation>Insira a senha aqui</translation>
    </message>
    <message id="whisperfish-unlock-button-label">
        <location filename="../qml/pages/UnlockPage.qml" line="85"/>
        <source>Unlock</source>
        <extracomment>unlock button label</extracomment>
        <translation>Desbloquear</translation>
    </message>
    <message id="whisperfish-password-info">
        <location filename="../qml/pages/SetupPasswordPage.qml" line="24"/>
        <source>Whisperfish stores identity keys, session state, and local message data encrypted on disk. The password you set is not stored anywhere and you will not be able to restore your data if you lose your password. Note: Attachments are currently stored unencrypted. You can disable storing of attachments in the Settings page.</source>
        <extracomment>Whisperfish password informational message</extracomment>
        <translation>O Whisperfish usa criptografia para salvar mensagens, chaves de identificação e o estado da sessão. A senha usada para criptografar não é armazenada em nenhum local e não será possível recuperar nenhum dado caso ela seja esquecida. Nota: atualmente os arquivos anexos são salvos não-criptografados, é possível entretanto desativar seu armazenamento no menu de configuração.</translation>
    </message>
    <message id="whisperfish-registration-message">
        <location filename="../qml/pages/RegisterPage.qml" line="17"/>
        <source>Enter the phone number you want to register with Signal.</source>
        <oldsource>Connect with Signal</oldsource>
        <extracomment>registration prompt text</extracomment>
        <translation>Insira seu número de telefone para criar sua conta no Signal.</translation>
    </message>
    <message id="whisperfish-registration-country-or-area">
        <location filename="../qml/pages/RegisterPage.qml" line="102"/>
        <source>Country or area</source>
        <extracomment>Label for country selection menu</extracomment>
        <translation>País ou área</translation>
    </message>
    <message id="whisperfish-not-selected">
        <location filename="../qml/pages/RegisterPage.qml" line="107"/>
        <source>Not selected</source>
        <extracomment>Placeholder for country not selected</extracomment>
        <translation>Não selecionado</translation>
    </message>
    <message id="whisperfish-share-contacts-label">
        <location filename="../qml/pages/RegisterPage.qml" line="230"/>
        <location filename="../qml/pages/Settings.qml" line="238"/>
        <source>Share Contacts</source>
        <extracomment>Share contacts label
----------
Settings page share contacts</extracomment>
        <translation>Compartilhar contatos</translation>
    </message>
    <message id="whisperfish-share-contacts-description">
        <location filename="../qml/pages/RegisterPage.qml" line="233"/>
        <location filename="../qml/pages/Settings.qml" line="241"/>
        <source>Allow Signal to use your local contact list, to find other Signal users.</source>
        <extracomment>Share contacts description</extracomment>
        <translation>Permitir que o Signal use a lista de contatos local para encontrar outros usuários.</translation>
    </message>
    <message id="whisperfish-verification-method-label">
        <location filename="../qml/pages/RegisterPage.qml" line="198"/>
        <source>Verification method</source>
        <extracomment>Verification method</extracomment>
        <translation>Método de verificação</translation>
    </message>
    <message id="whisperfish-use-voice-verification">
        <location filename="../qml/pages/RegisterPage.qml" line="216"/>
        <source>Use voice verification</source>
        <extracomment>Voice verification</extracomment>
        <translation>Ligação de voz</translation>
    </message>
    <message id="whisperfish-use-text-verification">
        <location filename="../qml/pages/RegisterPage.qml" line="211"/>
        <source>Use text verification</source>
        <extracomment>Text verification</extracomment>
        <translation>Código por SMS</translation>
    </message>
    <message id="whisperfish-registration-title">
        <location filename="../qml/pages/RegisterPage.qml" line="13"/>
        <location filename="../qml/pages/SetupRegistrationTypePage.qml" line="13"/>
        <source>Register</source>
        <extracomment>registration page title</extracomment>
        <translation>Criar conta</translation>
    </message>
    <message id="whisperfish-registration-retry-message">
        <location filename="../qml/pages/RegisterPage.qml" line="41"/>
        <source>Please retry with a valid phone number.</source>
        <extracomment>new registration prompt text asking to retry</extracomment>
        <translation>Tente novamente com um número de telefone válido.</translation>
    </message>
    <message id="whisperfish-registration-number-input-label">
        <location filename="../qml/pages/RegisterPage.qml" line="158"/>
        <source>Phone number</source>
        <extracomment>phone number input label</extracomment>
        <translation>Número de telefone</translation>
    </message>
    <message id="whisperfish-registration-number-input-placeholder">
        <location filename="../qml/pages/RegisterPage.qml" line="162"/>
        <source>Phone number</source>
        <extracomment>phone number input placeholder</extracomment>
        <translation>Insira o número aqui</translation>
    </message>
    <message id="whisperfish-voice-registration-directions">
        <location filename="../qml/pages/RegisterPage.qml" line="203"/>
        <source>Signal will call you with a 6-digit verification code. Please be ready to write it down.</source>
        <oldsource>Signal will call you with a 6-digit verification code. Please be ready to write this down.</oldsource>
        <extracomment>Registration directions</extracomment>
        <translation>O Signal vai ligar e informar um código de 6 dígitos necessário para terminar de criar a conta. Por favor anote-o.</translation>
    </message>
    <message id="whisperfish-text-registration-directions">
        <location filename="../qml/pages/RegisterPage.qml" line="205"/>
        <source>Signal will text you a 6-digit verification code.</source>
        <translation>O Signal irá enviar uma SMS com um código de 6 dígitos.</translation>
    </message>
    <message id="whisperfish-continue-button-label">
        <location filename="../qml/pages/RegisterPage.qml" line="246"/>
        <location filename="../qml/pages/SetupPasswordPage.qml" line="128"/>
        <location filename="../qml/pages/VerifyRegistrationPage.qml" line="138"/>
        <source>Continue</source>
        <extracomment>continue button label</extracomment>
        <translation>Continuar</translation>
    </message>
    <message id="whisperfish-reset-peer-accept">
        <location filename="../qml/pages/PeerIdentityChanged.qml" line="24"/>
        <location filename="../qml/pages/ResetPeerIdentity.qml" line="24"/>
        <source>Confirm</source>
        <extracomment>Reset peer identity accept text</extracomment>
        <translation>Confirmar</translation>
    </message>
    <message id="whisperfish-peer-not-trusted">
        <location filename="../qml/pages/PeerIdentityChanged.qml" line="32"/>
        <location filename="../qml/pages/ResetPeerIdentity.qml" line="32"/>
        <source>Peer identity is not trusted</source>
        <extracomment>Peer identity not trusted</extracomment>
        <translation>A identidade não é confiável</translation>
    </message>
    <message id="whisperfish-peer-not-trusted-message">
        <location filename="../qml/pages/PeerIdentityChanged.qml" line="42"/>
        <location filename="../qml/pages/ResetPeerIdentity.qml" line="42"/>
        <source>WARNING: %1 identity is no longer trusted. Tap Confirm to reset peer identity.</source>
        <extracomment>Peer identity not trusted message</extracomment>
        <translation>AVISO: a identidade de %1 já não é confiável. Toque em Confirmar para reiniciar sua identificação.</translation>
    </message>
    <message id="whisperfish-settings-linked-devices-menu">
        <location filename="../qml/pages/Settings.qml" line="30"/>
        <source>Linked Devices</source>
        <extracomment>Linked devices menu option</extracomment>
        <translation>Dispositivos associados</translation>
    </message>
    <message id="whisperfish-settings-reconnect-menu">
        <location filename="../qml/pages/Settings.qml" line="39"/>
        <source>Reconnect</source>
        <extracomment>Reconnect menu</extracomment>
        <translation>Reconectar</translation>
    </message>
    <message id="whisperfish-settings-title">
        <location filename="../qml/pages/Settings.qml" line="55"/>
        <source>Settings</source>
        <oldsource>Whisperfish Settings</oldsource>
        <extracomment>Settings page title</extracomment>
        <translation>Configurações</translation>
    </message>
    <message id="whisperfish-settings-identity-section-label">
        <location filename="../qml/pages/Settings.qml" line="62"/>
        <source>My Identity</source>
        <extracomment>Settings page My identity section label</extracomment>
        <translation>Minha identidade</translation>
    </message>
    <message id="whisperfish-settings-my-phone-number">
        <location filename="../qml/pages/Settings.qml" line="71"/>
        <source>My Phone</source>
        <oldsource>Phone</oldsource>
        <extracomment>Settings page My phone number</extracomment>
        <translation>Meu número</translation>
    </message>
    <message id="whisperfish-settings-my-uuid">
        <location filename="../qml/pages/Settings.qml" line="81"/>
        <source>My UUID registration number</source>
        <extracomment>Settings page My UUID</extracomment>
        <translation>Meu número UUID de conta</translation>
    </message>
    <message id="whisperfish-settings-identity-label">
        <location filename="../qml/pages/Settings.qml" line="92"/>
        <source>Identity</source>
        <extracomment>Settings page Identity label</extracomment>
        <translation>Identidade</translation>
    </message>
    <message id="whisperfish-settings-notifications-section">
        <location filename="../qml/pages/Settings.qml" line="101"/>
        <source>Notifications</source>
        <extracomment>Settings page notifications section</extracomment>
        <translation>Notificações</translation>
    </message>
    <message id="whisperfish-settings-notifications-enable">
        <location filename="../qml/pages/Settings.qml" line="125"/>
        <source>Enable notifications</source>
        <oldsource>Enabled</oldsource>
        <extracomment>Settings page notifications enable</extracomment>
        <translation>Criar notificações</translation>
    </message>
    <message id="whisperfish-settings-notifications-enable-description">
        <location filename="../qml/pages/Settings.qml" line="128"/>
        <source>If turned off, Whisperfish will not send any notification</source>
        <extracomment>Settings page notifications enable description</extracomment>
        <translation>Permite que o aplicativo crie notificações</translation>
    </message>
    <message id="whisperfish-settings-notifications-show-body">
        <location filename="../qml/pages/Settings.qml" line="141"/>
        <source>Show Message Body</source>
        <extracomment>Settings page notifications show message body</extracomment>
        <translation>Mostrar conteúdo da mensagem</translation>
    </message>
    <message id="whisperfish-settings-notifications-show-body-description">
        <location filename="../qml/pages/Settings.qml" line="144"/>
        <source>If turned off, Whisperfish will only show the sender of a message, not the contents.</source>
        <extracomment>Settings page notifications show message body description</extracomment>
        <translation>Permite que se mostre o conteúdo da mensagem ou somente seu remetente.</translation>
    </message>
    <message id="whisperfish-settings-general-section">
        <location filename="../qml/pages/Settings.qml" line="176"/>
        <source>General</source>
        <extracomment>Settings page general section</extracomment>
        <translation>Geral</translation>
    </message>
    <message id="whisperfish-settings-country-code">
        <location filename="../qml/pages/Settings.qml" line="184"/>
        <source>Country Code</source>
        <extracomment>Settings page country code</extracomment>
        <translation>Código de país</translation>
    </message>
    <message id="whisperfish-settings-country-code-description">
        <location filename="../qml/pages/Settings.qml" line="187"/>
        <source>The selected country code determines what happens when a local phone number is entered.</source>
        <extracomment>Settings page country code description</extracomment>
        <translation>Define o código usado por padrão para números de telefones locais.</translation>
    </message>
    <message id="whisperfish-settings-country-code-empty">
        <location filename="../qml/pages/Settings.qml" line="191"/>
        <source>none</source>
        <extracomment>settings page country code selection: nothing selected</extracomment>
        <translation>nenhum</translation>
    </message>
    <message id="whisperfish-settings-save-attachments">
        <location filename="../qml/pages/Settings.qml" line="218"/>
        <source>Save Attachments</source>
        <extracomment>Settings page save attachments</extracomment>
        <translation>Salvar anexos</translation>
    </message>
    <message id="whisperfish-settings-save-attachments-description">
        <location filename="../qml/pages/Settings.qml" line="222"/>
        <source>Attachments are stored at %1. Currently, when disabled, attachments will not work.</source>
        <oldsource>Attachments are stored at %1</oldsource>
        <extracomment>Settings page save attachments description</extracomment>
        <translation>Anexos são salvos em %1. Atualmente os anexos não funcionam quando esta opção está desativada.</translation>
    </message>
    <message id="whisperfish-settings-enable-enter-send">
        <location filename="../qml/pages/Settings.qml" line="255"/>
        <source>Return key send</source>
        <oldsource>EnterKey Send</oldsource>
        <extracomment>Settings page enable enter send</extracomment>
        <translation>Pressionar Enter para enviar</translation>
    </message>
    <message id="whisperfish-settings-enable-enter-send-description">
        <location filename="../qml/pages/Settings.qml" line="258"/>
        <source>When enabled, the return key functions as a send key. Otherwise, the return key can be used for multi-line messages.</source>
        <extracomment>Settings page enable enter send description</extracomment>
        <translation>Permite enviar a mensagem ao pressionar a tecla Enter. Caso contrário ela será usada para inserir saltos de linha.</translation>
    </message>
    <message id="whisperfish-settings-startup-shutdown-section">
        <location filename="../qml/pages/Settings.qml" line="279"/>
        <source>Autostart and Background</source>
        <extracomment>Settings page startup and shutdown section</extracomment>
        <translation>Funcionamento</translation>
    </message>
    <message id="whisperfish-settings-enable-autostart">
        <location filename="../qml/pages/Settings.qml" line="286"/>
        <source>Autostart after boot</source>
        <extracomment>Settings page enable autostart</extracomment>
        <translation>Iniciar automaticamente</translation>
    </message>
    <message id="whisperfish-settings-enable-background-mode-description">
        <location filename="../qml/pages/Settings.qml" line="323"/>
        <source>When enabled, Whisperfish keeps running in the background and can send notifications after the app window has been closed.</source>
        <oldsource>When enabled, Whisperfish starts automatically after each boot. If storage encryption is enabled or background-mode is off, the UI will be shown, otherwise the app starts in the background.</oldsource>
        <extracomment>Settings page enable background mode description</extracomment>
        <translation>Possibilita ao aplicativo criar notificações mesmo quando a janela dele estiver fechada.</translation>
    </message>
    <message id="whisperfish-settings-enable-background-mode">
        <location filename="../qml/pages/Settings.qml" line="320"/>
        <source>Background mode</source>
        <extracomment>Settings page enable background mode</extracomment>
        <translation>Modo segundo plano</translation>
    </message>
    <message id="whisperfish-settings-enable-typing-indicators">
        <location filename="../qml/pages/Settings.qml" line="108"/>
        <source>Enable typing indicators</source>
        <extracomment>Settings page use typing indicators</extracomment>
        <translation type="unfinished">Habilitar indicadores de digitação</translation>
    </message>
    <message id="whisperfish-settings-enable-typing-indicators-description">
        <location filename="../qml/pages/Settings.qml" line="111"/>
        <source>See when others are typing, and let others see when you are typing, if they also have this enabled.</source>
        <extracomment>Settings page scale image attachments description</extracomment>
        <translation type="unfinished">Veja quando os outros estão digitando, e deixe os outros verem quando você está digitando, se eles também tiverem isso habilitado.</translation>
    </message>
    <message id="whisperfish-settings-notifications-minimise">
        <location filename="../qml/pages/Settings.qml" line="157"/>
        <source>Minimise notifications</source>
        <extracomment>Settings page notifications show minimum number of notifications</extracomment>
        <translation>Minimizar notificações</translation>
    </message>
    <message id="whisperfish-settings-notifications-minimise-description">
        <location filename="../qml/pages/Settings.qml" line="160"/>
        <source>If turned on, Whisperfish will suppress all but the first notification from each session.</source>
        <extracomment>Settings page notifications show minimum number of notifications description</extracomment>
        <translation>Restringe a criação de notificação a uma por conversa.</translation>
    </message>
    <message id="whisperfish-settings-enable-autostart-description">
        <location filename="../qml/pages/Settings.qml" line="289"/>
        <source>When enabled, Whisperfish starts automatically after each boot. If storage encryption is enabled or background-mode is off, the UI will be shown, otherwise the app starts in the background.</source>
        <extracomment>Settings page enable autostart description</extracomment>
        <translation>Permite ao aplicativo iniciar automaticamente durante a inicialização do sistema. Ele iniciará oculto exceto se a senha para encriptar armazenamento exista ou o modo de segundo plano estiver desligado.</translation>
    </message>
    <message id="whisperfish-settings-autostart-manual-info">
        <location filename="../qml/pages/Settings.qml" line="313"/>
        <source>Whisperfish does not have the permission to change the autostart settings. You can enable or disable autostart manually from the command line by running &apos;systemctl --user enable harbour-whisperfish.service&apos; or &apos;systemctl --user disable harbour-whisperfish.service&apos;</source>
        <extracomment>Settings page info how to enable autostart manually</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-settings-quit-button">
        <location filename="../qml/pages/Settings.qml" line="340"/>
        <source>Quit Whisperfish</source>
        <extracomment>Settings page quit app button</extracomment>
        <translation>Fechar o Whisperfish</translation>
    </message>
    <message id="whisperfish-settings-advanced-section">
        <location filename="../qml/pages/Settings.qml" line="352"/>
        <source>Advanced</source>
        <extracomment>Settings page advanced section</extracomment>
        <translation>Avançado</translation>
    </message>
    <message id="whisperfish-settings-incognito-mode">
        <location filename="../qml/pages/Settings.qml" line="359"/>
        <source>Incognito Mode</source>
        <extracomment>Settings page incognito mode</extracomment>
        <translation>Modo Incógnito</translation>
    </message>
    <message id="whisperfish-settings-incognito-mode-description">
        <location filename="../qml/pages/Settings.qml" line="362"/>
        <source>Incognito Mode disables storage entirely. No attachments nor messages are saved, messages are visible until restart.</source>
        <extracomment>Settings page incognito mode description</extracomment>
        <translation>Restringe o aplicativo de salvar dados. Mensagens e anexos serão perdidos ao fechar o aplicativo.</translation>
    </message>
    <message id="whisperfish-settings-restarting-message">
        <location filename="../qml/pages/Settings.qml" line="370"/>
        <source>Restarting Whisperfish</source>
        <oldsource>Restart Whisperfish...</oldsource>
        <extracomment>Restart whisperfish remorse timer message (past tense)</extracomment>
        <translation>Reiniciando Whisperfish</translation>
    </message>
    <message id="whisperfish-settings-scale-image-attachments">
        <location filename="../qml/pages/Settings.qml" line="383"/>
        <source>Scale JPEG Attachments</source>
        <extracomment>Settings page scale image attachments</extracomment>
        <translation>Redimensionar anexos JPEG</translation>
    </message>
    <message id="whisperfish-settings-scale-image-attachments-description">
        <location filename="../qml/pages/Settings.qml" line="386"/>
        <source>Scale down JPEG attachments to save on bandwidth.</source>
        <extracomment>Settings page scale image attachments description</extracomment>
        <translation>Limita o tamanho dos arquivos JPEG para minimizar consumo de dados.</translation>
    </message>
    <message id="whisperfish-settings-debug-mode">
        <location filename="../qml/pages/Settings.qml" line="400"/>
        <source>Debug mode</source>
        <extracomment>Settings page: debug info toggle</extracomment>
        <translation>Modo de depuração</translation>
    </message>
    <message id="whisperfish-settings-debug-mode-description">
        <location filename="../qml/pages/Settings.qml" line="403"/>
        <source>Show debugging information and controls in the user interface.</source>
        <oldsource>Show debugging information in the user interface.</oldsource>
        <extracomment>Settings page: debug info toggle extended description</extracomment>
        <translation type="unfinished">Mostra informação para depuração na interface de usuário.</translation>
    </message>
    <message id="whisperfish-settings-compress-db">
        <location filename="../qml/pages/Settings.qml" line="418"/>
        <source>Compact database</source>
        <oldsource>Compress database</oldsource>
        <extracomment>Settings page &apos;Compact database&apos; button: execute &apos;VACUUM&apos; command on SQLite-database</extracomment>
        <translation>Comprimir base de dados</translation>
    </message>
    <message id="whisperfish-settings-stats-section">
        <location filename="../qml/pages/Settings.qml" line="429"/>
        <source>Statistics</source>
        <extracomment>Settings page stats section</extracomment>
        <translation>Estatísticas</translation>
    </message>
    <message id="whisperfish-settings-websocket">
        <location filename="../qml/pages/Settings.qml" line="434"/>
        <source>Websocket Status</source>
        <extracomment>Settings page websocket status</extracomment>
        <translation>Estado do Websocket</translation>
    </message>
    <message id="whisperfish-settings-connected">
        <location filename="../qml/pages/Settings.qml" line="438"/>
        <source>Connected</source>
        <extracomment>Settings page connected message</extracomment>
        <translation>Conectado</translation>
    </message>
    <message id="whisperfish-settings-disconnected">
        <location filename="../qml/pages/Settings.qml" line="441"/>
        <source>Disconnected</source>
        <extracomment>Settings page disconnected message</extracomment>
        <translation>Desconectado</translation>
    </message>
    <message id="whisperfish-settings-unsent-messages">
        <location filename="../qml/pages/Settings.qml" line="446"/>
        <source>Unsent Messages</source>
        <extracomment>Settings page unsent messages</extracomment>
        <translation>Mensagens não enviadas</translation>
    </message>
    <message id="whisperfish-settings-total-sessions">
        <location filename="../qml/pages/Settings.qml" line="452"/>
        <source>Total Sessions</source>
        <extracomment>Settings page total sessions</extracomment>
        <translation>Número total de sessões</translation>
    </message>
    <message id="whisperfish-settings-total-messages">
        <location filename="../qml/pages/Settings.qml" line="458"/>
        <source>Total Messages</source>
        <extracomment>Settings page total messages</extracomment>
        <translation>Número total de mensagens</translation>
    </message>
    <message id="whisperfish-settings-total-contacts">
        <location filename="../qml/pages/Settings.qml" line="464"/>
        <source>Signal Contacts</source>
        <extracomment>Settings page total signal contacts</extracomment>
        <translation>Contatos no Signal</translation>
    </message>
    <message id="whisperfish-settings-encrypted-keystore">
        <location filename="../qml/pages/Settings.qml" line="470"/>
        <source>Encrypted Key Store</source>
        <extracomment>Settings page encrypted key store</extracomment>
        <translation>Criptografia das chaves armazenadas</translation>
    </message>
    <message id="whisperfish-settings-encrypted-keystore-enabled">
        <location filename="../qml/pages/Settings.qml" line="474"/>
        <source>Enabled</source>
        <extracomment>Settings page encrypted key store enabled</extracomment>
        <translation>Ligada</translation>
    </message>
    <message id="whisperfish-settings-encrypted-keystore-disabled">
        <location filename="../qml/pages/Settings.qml" line="477"/>
        <source>Disabled</source>
        <extracomment>Settings page encrypted key store disabled</extracomment>
        <translation>Desligada</translation>
    </message>
    <message id="whisperfish-settings-encrypted-db">
        <location filename="../qml/pages/Settings.qml" line="482"/>
        <source>Encrypted Database</source>
        <extracomment>Settings page encrypted database</extracomment>
        <translation>Criptografia da Base de dados</translation>
    </message>
    <message id="whisperfish-settings-encrypted-db-enabled">
        <location filename="../qml/pages/Settings.qml" line="486"/>
        <source>Enabled</source>
        <extracomment>Settings page encrypted db enabled</extracomment>
        <translation>Ligada</translation>
    </message>
    <message id="whisperfish-settings-encrypted-db-disabled">
        <location filename="../qml/pages/Settings.qml" line="489"/>
        <source>Disabled</source>
        <extracomment>Settings page encrypted db disabled</extracomment>
        <translation>Desligada</translation>
    </message>
    <message id="whisperfish-verify-contact-identity-title">
        <location filename="../qml/pages/VerifyIdentity.qml" line="110"/>
        <source>Verify safety numbers</source>
        <oldsource>Verify %1</oldsource>
        <extracomment>Verify safety numbers</extracomment>
        <translation>Verificar números de segurança</translation>
    </message>
    <message id="whisperfish-group-add-member-menu">
        <location filename="../qml/pages/GroupProfilePage.qml" line="243"/>
        <source>Add Member</source>
        <extracomment>Add group member menu item</extracomment>
        <translation>Adicionar membro</translation>
    </message>
    <message id="whisperfish-unknown-contact">
        <location filename="../qml/pages/GroupProfilePage.qml" line="61"/>
        <source>Unknown</source>
        <extracomment>Unknown contact in group member list</extracomment>
        <translation>Desconhecido</translation>
    </message>
    <message id="whisperfish-group-updated-to-groupv2">
        <location filename="../qml/pages/GroupProfilePage.qml" line="96"/>
        <source>Updated to the new group format</source>
        <extracomment>Indicator for updated groups</extracomment>
        <translation>Grupo atualizado para o novo formato</translation>
    </message>
    <message id="whisperfish-group-not-updated-to-groupv2">
        <location filename="../qml/pages/GroupProfilePage.qml" line="99"/>
        <source>Not updated to the new group format</source>
        <extracomment>Indicator for not yet updated groups</extracomment>
        <translation>Grupo não atualizado para o novo formato</translation>
    </message>
    <message id="whisperfish-group-refresh">
        <location filename="../qml/pages/GroupProfilePage.qml" line="206"/>
        <source>Refresh group</source>
        <extracomment>Refresh group menu item</extracomment>
        <translation>Atualizar grupo</translation>
    </message>
    <message id="whisperfish-group-leave-menu">
        <location filename="../qml/pages/GroupProfilePage.qml" line="215"/>
        <source>Leave this group</source>
        <oldsource>Leave</oldsource>
        <extracomment>Leave group menu item</extracomment>
        <translation>Sair deste grupo</translation>
    </message>
    <message id="whisperfish-group-leave-remorse">
        <location filename="../qml/pages/GroupProfilePage.qml" line="223"/>
        <source>Left group and deleted all messages</source>
        <oldsource>Leaving group and removing ALL messages!</oldsource>
        <extracomment>Leave group remorse message (past tense)</extracomment>
        <translation>Toque para cancelar saída do grupo e evitar que se apaguem todas a suas mensagens</translation>
    </message>
    <message id="whisperfish-group-invite-link-menu">
        <location filename="../qml/pages/GroupProfilePage.qml" line="236"/>
        <source>Create invitation link</source>
        <extracomment>Create invite link menu item</extracomment>
        <translation>Criar link para convite</translation>
    </message>
    <message id="whisperfish-group-member-menu-direct-message">
        <location filename="../qml/pages/GroupProfilePage.qml" line="274"/>
        <source>Message to %1</source>
        <extracomment>Menu item to start a private chat with a group member</extracomment>
        <translation>Mensagem à %1</translation>
    </message>
    <message id="whisperfish-group-member-menu-save-contact">
        <location filename="../qml/pages/GroupProfilePage.qml" line="283"/>
        <source>Add to contacts</source>
        <extracomment>Menu item to save a group member to the local address book</extracomment>
        <translation>Adicionar aos contatos</translation>
    </message>
    <message id="whisperfish-group-member-menu-verify-fingerprint">
        <location filename="../qml/pages/GroupProfilePage.qml" line="290"/>
        <source>Verify safety number</source>
        <extracomment>Menu item to verify safety numbers with a group member</extracomment>
        <translation>Verificar número de segurança</translation>
    </message>
    <message id="whisperfish-group-member-menu-remove-from-group">
        <location filename="../qml/pages/GroupProfilePage.qml" line="304"/>
        <source>Remove from this group</source>
        <extracomment>Menu item to remove a member from a group (requires admin privileges)</extracomment>
        <translation>Remover deste grupo</translation>
    </message>
    <message id="whisperfish-group-member-name-self">
        <location filename="../qml/pages/GroupProfilePage.qml" line="339"/>
        <source>You</source>
        <extracomment>Title for the user&apos;s entry in a list of group members</extracomment>
        <translation>Você</translation>
    </message>
    <message id="whisperfish-info-page-default-title">
        <location filename="../qml/components/BlockingInfoPageBase.qml" line="17"/>
        <source>Whisperfish</source>
        <extracomment>default title of full-screen info pages (below the icon)</extracomment>
        <translation>Whisperfish</translation>
    </message>
    <message id="whisperfish-fatal-error-title">
        <location filename="../qml/pages/FatalErrorPage.qml" line="11"/>
        <source>Error</source>
        <extracomment>fatal error page title</extracomment>
        <translation>Erro</translation>
    </message>
    <message id="whisperfish-fatal-error-hint">
        <location filename="../qml/pages/FatalErrorPage.qml" line="18"/>
        <source>Please restart Whisperfish. If the problem persists and appears to be an issue with Whisperfish, please report the issue.</source>
        <extracomment>generic hint on what to do after a fatal error occurred (error message will be shown separately)</extracomment>
        <translation>É necessário reiniciar o aplicativo. Se o problema persistir e parecer ser um erro do Whisperfish por favor o reporte.</translation>
    </message>
    <message id="whisperfish-startup-placeholder-title">
        <location filename="../qml/pages/LandingPage.qml" line="73"/>
        <source>Welcome</source>
        <extracomment>welcome text shown when startup takes a long time</extracomment>
        <translation>Bem-vindo</translation>
    </message>
    <message id="whisperfish-verify-page-title">
        <location filename="../qml/pages/VerifyRegistrationPage.qml" line="10"/>
        <source>Verify</source>
        <extracomment>verify registration page title</extracomment>
        <translation>Confirmar</translation>
    </message>
    <message id="whisperfish-verify-code-prompt">
        <location filename="../qml/pages/VerifyRegistrationPage.qml" line="13"/>
        <source>Please enter the code you received from Signal.</source>
        <extracomment>verify registration prompt</extracomment>
        <translation>Por favor digite o código de 6 dígitos.</translation>
    </message>
    <message id="whisperfish-verify-instructions-voice">
        <location filename="../qml/pages/VerifyRegistrationPage.qml" line="20"/>
        <source>Signal should have called you with a a 6-digit verification code. Please wait a moment, or restart the process if you have not received a call.</source>
        <extracomment>verify registration instructions: voice</extracomment>
        <translation>O código de verificação de 6 dígitos do Signal será informado através de uma ligação. Caso não a receba em alguns minutos por favor reinicie o processo.</translation>
    </message>
    <message id="whisperfish-verify-instructions-sms">
        <location filename="../qml/pages/VerifyRegistrationPage.qml" line="25"/>
        <source>Signal should have sent you a 6-digit verification code via text message. Please wait a moment, or restart the process if you have not received a message.</source>
        <extracomment>verify registration instructions: text message</extracomment>
        <translation>O código de verificação de 6 dígitos do Signal será enviado por SMS. Caso não o receba em alguns minutos por favor reinicie o processo.</translation>
    </message>
    <message id="whisperfish-verify-retry-prompt">
        <location filename="../qml/pages/VerifyRegistrationPage.qml" line="43"/>
        <source>Please retry with a valid code.</source>
        <extracomment>verification: prompt to retry with a new code</extracomment>
        <translation>Tente inserir novamente o código de 6 dígitos.</translation>
    </message>
    <message id="whisperfish-verify-code-input-label">
        <location filename="../qml/pages/VerifyRegistrationPage.qml" line="84"/>
        <source>Verification code</source>
        <extracomment>verification code input label</extracomment>
        <translation>Código de verificação</translation>
    </message>
    <message id="whisperfish-verify-code-input-placeholder">
        <location filename="../qml/pages/VerifyRegistrationPage.qml" line="87"/>
        <source>Code</source>
        <extracomment>verification code input placeholder</extracomment>
        <translation>Insira aqui o código de 6 dígitos</translation>
    </message>
    <message id="whisperfish-attachment-preview-contact-title">
        <location filename="../qml/components/attachment/AttachmentItemContact.qml" line="22"/>
        <source>Shared contact</source>
        <extracomment>Placeholder shown as title for an attached contact in a message</extracomment>
        <translation>Contato compartilhado</translation>
    </message>
    <message id="whisperfish-chat-input-attachment-label" numerus="yes">
        <location filename="../qml/components/ChatTextInput.qml" line="215"/>
        <source>%n attachment(s)</source>
        <extracomment>Number of attachments currently selected for sending</extracomment>
        <translation>
            <numerusform>%n anexo</numerusform>
            <numerusform>%n anexos</numerusform>
        </translation>
    </message>
    <message id="whisperfish-chat-input-placeholder-personal">
        <location filename="../qml/components/ChatTextInput.qml" line="223"/>
        <source>Hi %1</source>
        <extracomment>Personalized placeholder for chat input, e.g. &quot;Hi John&quot;</extracomment>
        <translation>Olá %1</translation>
    </message>
    <message id="whisperfish-chat-input-placeholder-default">
        <location filename="../qml/components/ChatTextInput.qml" line="227"/>
        <source>Write a message</source>
        <extracomment>Generic placeholder for chat input</extracomment>
        <translation>Escreva a mensagem aqui</translation>
    </message>
    <message id="whisperfish-select-attachments-page-title">
        <location filename="../qml/components/ChatTextInput.qml" line="374"/>
        <source>Select attachments</source>
        <extracomment>Attachment picker page title</extracomment>
        <translation>Selecionar anexos</translation>
    </message>
    <message id="whisperfish-attachments-plus-n" numerus="yes">
        <location filename="../qml/components/message/AttachmentsLoader.qml" line="222"/>
        <source>+%n</source>
        <extracomment>Label hinting at more attachments than are currently shown. Read as &quot;and %n more&quot;.</extracomment>
        <translation>
            <numerusform>+%n</numerusform>
            <numerusform>+%n</numerusform>
        </translation>
    </message>
    <message id="whisperfish-attachments-loader-show-more" numerus="yes">
        <location filename="../qml/components/message/AttachmentsLoader.qml" line="289"/>
        <source>and %n more</source>
        <oldsource>and %1 more</oldsource>
        <extracomment>Note if some message attachments are hidden instead of being shown inline</extracomment>
        <translation>
            <numerusform>e um mais</numerusform>
            <numerusform>e outros %n</numerusform>
        </translation>
    </message>
    <message id="whisperfish-message-no-timestamp">
        <location filename="../qml/components/message/InfoRow.qml" line="41"/>
        <source>no time</source>
        <extracomment>Placeholder note if a message doesn&apos;t have a timestamp (which must not happen).</extracomment>
        <translation>data desconhecida</translation>
    </message>
    <message id="whisperfish-message-show-less">
        <location filename="../qml/components/message/InfoRow.qml" line="78"/>
        <source>show less</source>
        <extracomment>Hint for very long messages, while expanded</extracomment>
        <translation>mostrar menos</translation>
    </message>
    <message id="whisperfish-message-show-more">
        <location filename="../qml/components/message/InfoRow.qml" line="81"/>
        <source>show more</source>
        <extracomment>Hint for very long messages, while not expanded</extracomment>
        <translation>mostrar mais</translation>
    </message>
    <message id="whisperfish-sender-name-label-outgoing">
        <location filename="../qml/components/QuotedMessagePreview.qml" line="98"/>
        <location filename="../qml/components/SenderNameLabel.qml" line="81"/>
        <source>You</source>
        <extracomment>Name shown when replying to own messages</extracomment>
        <translation>Você</translation>
    </message>
    <message id="whisperfish-quoted-message-preview-attachment">
        <location filename="../qml/components/QuotedMessagePreview.qml" line="121"/>
        <source>Attachment</source>
        <extracomment>Placeholder text if quoted message preview contains no text, only attachments</extracomment>
        <translation>Anexo</translation>
    </message>
    <message id="whisperfish-sender-label-empty">
        <location filename="../qml/delegates/MessageDelegate.qml" line="182"/>
        <source>no sender</source>
        <extracomment>Label shown if a message doesn&apos;t have a sender.</extracomment>
        <translation>sem remetente</translation>
    </message>
    <message id="whisperfish-message-empty-note">
        <location filename="../qml/delegates/MessageDelegate.qml" line="237"/>
        <source>this message is empty</source>
        <extracomment>Placeholder note if an empty message is encountered.</extracomment>
        <translation>mensagem vazia</translation>
    </message>
    <message id="whisperfish-service-message-joined-group-self">
        <location filename="../qml/delegates/ServiceMessageDelegate.qml" line="49"/>
        <source>You joined the group “%1”.</source>
        <extracomment>Service message, %1 = group name</extracomment>
        <translation>Você entrou no grupo &quot;%1&quot;.</translation>
    </message>
    <message id="whisperfish-service-message-left-group-self">
        <location filename="../qml/delegates/ServiceMessageDelegate.qml" line="53"/>
        <source>You left the group.</source>
        <extracomment>Service message</extracomment>
        <translation>Você saiu do grupo.</translation>
    </message>
    <message id="whisperfish-service-message-joined-group-peer">
        <location filename="../qml/delegates/ServiceMessageDelegate.qml" line="57"/>
        <source>%1 joined the group.</source>
        <extracomment>Service message, %1 is the new member&apos;s name</extracomment>
        <translation>%1 entrou no grupo.</translation>
    </message>
    <message id="whisperfish-service-message-left-group-peer">
        <location filename="../qml/delegates/ServiceMessageDelegate.qml" line="61"/>
        <source>%1 left the group.</source>
        <extracomment>Service message, %1 is is the lost member&apos;s name</extracomment>
        <translation>%1 saiu do grupo.</translation>
    </message>
    <message id="whisperfish-service-message-missed-call-voice">
        <location filename="../qml/delegates/ServiceMessageDelegate.qml" line="65"/>
        <source>You missed a call from %1.</source>
        <extracomment>Service message, %1 is a name</extracomment>
        <translation>Há uma chamada perdida de %1.</translation>
    </message>
    <message id="whisperfish-service-message-missed-call-video">
        <location filename="../qml/delegates/ServiceMessageDelegate.qml" line="69"/>
        <source>You missed a video call from %1.</source>
        <extracomment>Service message, %1 is a name</extracomment>
        <translation>Há uma videochamada perdida de %1.</translation>
    </message>
    <message id="whisperfish-service-message-call-voice-self">
        <location filename="../qml/delegates/ServiceMessageDelegate.qml" line="73"/>
        <source>You called %1.</source>
        <extracomment>Service message, %1 is a name</extracomment>
        <translation>Você ligou para %1.</translation>
    </message>
    <message id="whisperfish-service-message-call-video-self">
        <location filename="../qml/delegates/ServiceMessageDelegate.qml" line="77"/>
        <source>You started a video call with %1.</source>
        <extracomment>Service message, %1 is a name</extracomment>
        <translation>Você iniciou uma videochamada com %1.</translation>
    </message>
    <message id="whisperfish-service-message-call-voice-peer">
        <location filename="../qml/delegates/ServiceMessageDelegate.qml" line="81"/>
        <source>%1 called you.</source>
        <extracomment>Service message, %1 is a name</extracomment>
        <translation>%1 ligou.</translation>
    </message>
    <message id="whisperfish-service-message-call-video-peer">
        <location filename="../qml/delegates/ServiceMessageDelegate.qml" line="85"/>
        <source>%1 started a video call with you.</source>
        <extracomment>Service message, %1 is a name</extracomment>
        <translation>%1 iniciou uma videochamada.</translation>
    </message>
    <message id="whisperfish-service-message-fingerprint-changed">
        <location filename="../qml/delegates/ServiceMessageDelegate.qml" line="90"/>
        <source>Your safety number with %1 has changed. Swipe right to verify the new number.</source>
        <extracomment>Service message, %1 is a name</extracomment>
        <translation>O seu número de segurança com %1 mudou. Deslize para a direita para verificá-lo.</translation>
    </message>
    <message id="whisperfish-service-message-session-reset-self">
        <location filename="../qml/delegates/ServiceMessageDelegate.qml" line="94"/>
        <source>You have reset the secure session with %1.</source>
        <extracomment>Service message, %1 is a name</extracomment>
        <translation>Você reiniciou a sessão segura com %1.</translation>
    </message>
    <message id="whisperfish-service-message-session-reset-peer">
        <location filename="../qml/delegates/ServiceMessageDelegate.qml" line="98"/>
        <source>%1 has reset the secure session with you.</source>
        <extracomment>Service message, %1 is a name</extracomment>
        <translation>%1 reiniciou a sessão segura com você.</translation>
    </message>
    <message id="whisperfish-service-message-not-supported">
        <location filename="../qml/delegates/ServiceMessageDelegate.qml" line="103"/>
        <source>This service message is not yet supported by Whisperfish. Please file a bug report. (Type: &apos;%1&apos;.)</source>
        <extracomment>Service message, %1 is an internal message type identifier</extracomment>
        <translation>Esta mensagem de serviço ainda não é suportada pelo Whisperfish. Por favor reporte esse erro. (Tipo: &apos;%1&apos;.)</translation>
    </message>
    <message id="whisperfish-service-message-more-info">
        <location filename="../qml/delegates/ServiceMessageDelegate.qml" line="160"/>
        <source>more information</source>
        <translation>mais informações</translation>
    </message>
    <message id="whisperfish-contact-card-page-save">
        <location filename="../qml/pages/ContactCardPage.qml" line="52"/>
        <source>Save to address book</source>
        <extracomment>Menu item to save a shared contact to the local address book</extracomment>
        <translation>Salvar na lista de contatos</translation>
    </message>
    <message id="whisperfish-expanded-message-page-header">
        <location filename="../qml/pages/ExpandedMessagePage.qml" line="57"/>
        <source>Full message</source>
        <extracomment>Page title for a very long message shown on a page of its own</extracomment>
        <translation>Mensagem completa</translation>
    </message>
    <message id="whisperfish-expanded-message-info-outbound">
        <location filename="../qml/pages/ExpandedMessagePage.qml" line="61"/>
        <source>to %1</source>
        <extracomment>Page description for a very long message shown on a page of its own</extracomment>
        <translation>para %1</translation>
    </message>
    <message id="whisperfish-expanded-message-info-inbound">
        <location filename="../qml/pages/ExpandedMessagePage.qml" line="64"/>
        <source>from %1</source>
        <extracomment>Page description for a very long message shown on a page of its own</extracomment>
        <translation>de %1</translation>
    </message>
    <message id="whisperfish-view-image-page-loading">
        <location filename="../qml/pages/ViewImagePage.qml" line="251"/>
        <source>Loading image</source>
        <extracomment>Full page placeholder shown while a large image is being loaded</extracomment>
        <translation>Carregando imagem</translation>
    </message>
    <message id="whisperfish-view-image-page-error">
        <location filename="../qml/pages/ViewImagePage.qml" line="261"/>
        <source>Failed to load</source>
        <extracomment>Full page placeholder shown when an image failed to load</extracomment>
        <translation>Falha ao carregar</translation>
    </message>
    <message id="whisperfish-view-video-page-error">
        <location filename="../qml/pages/ViewVideoPage.qml" line="107"/>
        <source>Failed to play</source>
        <extracomment>Full page placeholder shown when a video failed to load</extracomment>
        <translation>Falha ao reproduzir</translation>
    </message>
    <message id="whisperfish-message-actions-info-label" numerus="yes">
        <location filename="../qml/pages/ConversationPage.qml" line="279"/>
        <source>%n message(s) selected</source>
        <oldsource>%1 message(s) selected</oldsource>
        <extracomment>Info label shown while selecting messages</extracomment>
        <translation>
            <numerusform>%n mensagem selecionada</numerusform>
            <numerusform>%n mensagens selecionadas</numerusform>
        </translation>
    </message>
    <message id="whisperfish-message-action-clear-selection" numerus="yes">
        <location filename="../qml/pages/ConversationPage.qml" line="303"/>
        <source>Clear selection</source>
        <extracomment>Message action description, shown if one or more messages are selected</extracomment>
        <translation>
            <numerusform>Cancelar seleção</numerusform>
            <numerusform>Cancelar seleção</numerusform>
        </translation>
    </message>
    <message id="whisperfish-message-action-copy" numerus="yes">
        <location filename="../qml/pages/ConversationPage.qml" line="312"/>
        <source>Copy %n message(s)</source>
        <oldsource>Copy %1 message(s)</oldsource>
        <extracomment>Message action description</extracomment>
        <translation>
            <numerusform>Copiar %n mensagem</numerusform>
            <numerusform>Copiar %n mensagens</numerusform>
        </translation>
    </message>
    <message id="whisperfish-message-action-info">
        <location filename="../qml/pages/ConversationPage.qml" line="320"/>
        <source>Show message info</source>
        <extracomment>Message action description (only available if n==1)</extracomment>
        <translation>Mostrar informações sobre mensagem</translation>
    </message>
    <message id="whisperfish-message-action-delete-for-self" numerus="yes">
        <location filename="../qml/pages/ConversationPage.qml" line="334"/>
        <source>Locally delete %n message(s)</source>
        <oldsource>Delete %1 message(s) for me</oldsource>
        <extracomment>Message action description</extracomment>
        <translation>
            <numerusform>Apagar mensagem nesse dispositivo</numerusform>
            <numerusform>Apagar %n mensagens nesse dispositivo</numerusform>
        </translation>
    </message>
    <message id="whisperfish-message-action-delete-for-all" numerus="yes">
        <location filename="../qml/pages/ConversationPage.qml" line="343"/>
        <source>Delete %n message(s) for all</source>
        <oldsource>Delete %1 message(s) for all</oldsource>
        <extracomment>Message action description</extracomment>
        <translation>
            <numerusform>Apagar mensagem em todos os dispositivos</numerusform>
            <numerusform>Apagar %n mensagens em todos os dispositivos</numerusform>
        </translation>
    </message>
    <message id="whisperfish-message-action-resend" numerus="yes">
        <location filename="../qml/pages/ConversationPage.qml" line="355"/>
        <source>Retry sending (the) failed message(s)</source>
        <extracomment>Message action description</extracomment>
        <translation>
            <numerusform>Tentar enviar mensagem novamente</numerusform>
            <numerusform>Tentar enviar mensagens novamente</numerusform>
        </translation>
    </message>
    <message id="whisperfish-share-page-title">
        <location filename="../qml/pages/ShareDestinationV1.qml" line="28"/>
        <location filename="../qml/pages/ShareDestinationV2.qml" line="27"/>
        <source>Share contents</source>
        <extracomment>Title of the page to select recipients and send a shared file</extracomment>
        <translation>Compartilhar conteúdo</translation>
    </message>
    <message id="whisperfish-signal-captcha">
        <location filename="../qml/pages/RegistrationCaptcha.qml" line="37"/>
        <source>Signal Captcha</source>
        <extracomment>Registration captcha page title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-cover-unread-label" numerus="yes">
        <location filename="../qml/cover/CoverPage.qml" line="41"/>
        <source>Unread&lt;br/&gt;message(s)</source>
        <extracomment>Unread messages count cover label. Code requires exact line break tag &quot;&lt;br/&gt;&quot;.</extracomment>
        <translation>
            <numerusform>Mensagem&lt;br/&gt;não lida</numerusform>
            <numerusform>Mensagens&lt;br/&gt;não lidas</numerusform>
        </translation>
    </message>
    <message id="whisperfish-registration-type-message">
        <location filename="../qml/pages/SetupRegistrationTypePage.qml" line="17"/>
        <source>Do you want to register whisperfish as primariy device or link it as secondary device to an existing signal app?</source>
        <extracomment>registration type prompt text</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-register-primary-button-label">
        <location filename="../qml/pages/SetupRegistrationTypePage.qml" line="48"/>
        <source>Primary device</source>
        <extracomment>register as primary device button label</extracomment>
        <translation>Dispositivo principal</translation>
    </message>
    <message id="whisperfish-register-secondary-button-label">
        <location filename="../qml/pages/SetupRegistrationTypePage.qml" line="57"/>
        <source>Secondary device</source>
        <extracomment>link as secondary device button label</extracomment>
        <translation>Dispositivo secundário</translation>
    </message>
</context>
</TS>
